# Банкомат  
Актуальная структура запросов  
/bank/set (POST)- Загрузка купюр в формате JSON {"100": 1, "10": 2 }  
/bank/status (GET)- Запрос текущего состояния банкомата  
/bank/withdraw (POST)- Запрос на выдачу купюр {"amount" : 12345}  
  
Для данного проекта более адекватным видится применение кастомных статусов:  
  
Коды ответов в поле status:  
1 - ОК  
2 - Ошибка  
  
Так же для некоторых ситуаций был реализован возврат 400 (Bad Request) ошибки на уровне HTTP на всякий случай  
