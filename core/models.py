from django.db import models
from django.contrib.postgres.fields import JSONField

class Banknote(models.Model):
    nominal = models.IntegerField()
    amount = models.IntegerField()

class Transaction(models.Model):
    (SET, STATUS, WITHDRAW, ) = range(1, 4)
    ACTIONS = (
        (SET, 'Загружено', ),
        (STATUS, 'Текущее состояние', ),
        (WITHDRAW, 'Выдано', ),
    )

    date = models.DateTimeField(auto_now=True)
    action = models.IntegerField(
        choices=ACTIONS,
        null=False,
        default=STATUS
    )
    params = JSONField(null=True)