from django.contrib import admin
from .models import (
    Banknote,
    Transaction,
)
# Register your models here.
@admin.register(Banknote)
class BanknotesAdmin(admin.ModelAdmin):
    list_display = (
        'nominal',
        'amount',
    )
	
@admin.register(Transaction)
class TransactionsAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'action',
		'params'
    )
