from django.shortcuts import render
from django.utils.decorators import method_decorator
from bank.settings import *

from .models import (
	Banknote,
	Transaction
)
from django.http import (
    HttpResponse,
    JsonResponse,
)

def json_response(data, status=200):
    return JsonResponse(
        data,
        status=status,
        content_type='application/json; encoding=utf-8',
    )

from django.views import View
import json

class SetView(View):
	def post(self, request, format=None):
		try:
			data = json.loads(request.body)
		except Exception as e:
			return json_response({
				'status':ERROR_STATUS,
				'data':str(e),
			}, 400)	
			
		for k,v in data.items():
			try:
				obj = Banknote.objects.filter(nominal=k)
			except Exception as e:
				return json_response({
					'status':ERROR_STATUS,
					'data':str(e),
				}, 400)			
			if obj:
				obj[0].amount = obj[0].amount + v
				obj[0].save()
			else:
				try:
					obj = Banknote(
						nominal = k,
						amount = v
					)
				except Exception as e:
					return json_response({
						'status':ERROR_STATUS,
						'data':str(e),
					}, 400)						
				obj.save()
			
		return json_response({
			'status':OK_STATUS,
			'data':'Купюры успешно загружены',
		})
			
class StatusView(View):
	def get(self, request, format=None):
		response = {}
		
		for b in Banknote.objects.all():
			response.update({b.nominal: b.amount})
			
		return json_response({
			'status':OK_STATUS,
			'data':response,
		})
		
class WithdrawView(View):
	def post(self, request, format=None):
		data = json.loads(request.body)
		reminder = int(data['amount'])
		banknotes = Banknote.objects.all()
		array = []
		banknote_arr = []
		response = {}

		for b in banknotes:
			if b.amount != 0:
				array.append(b.nominal)

		nominal_arr = sorted(array, reverse=True)
		i = 0;
		
		for n in nominal_arr:
			banknote_arr.append(banknotes.get(nominal=n))
			b = banknote_arr[i]
			quantity = reminder // n
			
			if b.amount >= quantity:
				b.amount = b.amount - quantity
			else:
				quantity = b.amount
				b.amount = 0
			
			response[n] = quantity
			reminder = reminder - n*quantity
			i += 1
			
		if reminder > 0:
			return json_response({
				'status':ERROR_STATUS,
				'data': "Недостаточно банкнот"
			}, 400)
		else:
			for b in banknote_arr:
				b.save()
				
			return json_response({
				'status':OK_STATUS,
				'data':response,
			})				