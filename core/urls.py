from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from core.views import (
    StatusView,
    SetView,
    WithdrawView
)
urlpatterns = [
    url(r'^status$', StatusView.as_view()),
    url(r'^set$', SetView.as_view()),
    url(r'^withdraw$', WithdrawView.as_view())
]
